use pmcro::log_info;


struct Product {
  name: String,
  price: u32
}

fn main() {
  log_info!([TIME] starting program ...);

  let laptop = Product { name: "Macbook pro".to_owned(), price: 200 };
  buy_product(laptop, 20);
}

#[log_call]
fn buy_product(product: Product, discount: u32) {
  //..
}