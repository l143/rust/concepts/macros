extern crate proc_macro;

use proc_macro::TokenStream;
use quote::quote;
use chrono::prelude::*;

use syn::{self, parse_macro_input, AttributeArgs, ItemFn};
use darling::FromMeta;

/*
Three types of procedural macros in rust
 - function, 
 - atribute 
 - custom derive
*/
#[proc_macro]
pub fn log_info(input: TokenStream) -> TokenStream {
    let mut output: String = "[Info]".to_owned();

    for token in input {
        let token_string = token.to_string();

        match token_string.as_str() {
            "[TIME]" => {
                let time = Utc::now().time().to_string();
                output.push_str(&format!("{} ", time));
            },
            _ => {
                output.push_str(&format!("{} ", token_string));
            }
        }
    }

    TokenStream::from(quote! {
        println|!("{}", #output);
    })
}


#[derive(FromMeta)]
struct MacroArgs {
    #[darling(default)]
    verbose: bool
}

#[proc_macro_attribute]
pub fn log_call(args: TokenStream, input: TokenStream) -> TokenStream {
    let attr_args = parse_macro_input!(args as AttributeArgs);
    let mut input = parse_macro_input!(input as ItemFn);
    
    let attr_args = match MacroArgs::from_list(&attr_args) {
        Ok(v) => v,
        Err(e) => { return TokenStream::from(e.write_errors()) }
    };
}